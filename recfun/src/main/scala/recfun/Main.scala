package recfun

object Main {
  def main(args: Array[String]) {
    /*
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
    */
    /*
    def test(s: String): Unit = {
      println(s + " = " + balance(s.toList))
    }
    test("()")
    test(":-)")
    test("())(")
    test("I told him (that it’s not (yet) done). (But he wasn’t listening)")
    test("(if (zero? x) max (/ 1 x))")
    */
    /*
    def test(money: Int, coins: List[Int]): Unit = {
      println(s"Money [$money], coins [$coins]")
      println(countChange(money, coins))
    }

    test(4, List(1, 2))
    test(4, List())
    test(0, List(1, 2))
    */
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = {
      if (c == 0 || r == 0 || c == r) 1
      else pascal(c - 1, r - 1) + pascal(c, r - 1)
    }
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {
      def balance_(charsRemaining: List[Char], depth: Int): Boolean = {
        if (depth < 0) false
        else if (charsRemaining.isEmpty) depth == 0
        else if (charsRemaining.head == '(') balance_(charsRemaining.tail, depth + 1)
        else if (charsRemaining.head == ')') balance_(charsRemaining.tail, depth - 1)
        else balance_(charsRemaining.tail, depth)
      }
      balance_(chars, 0)
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {
      def enumCoins(i: Int, moneyRemaining: Int): Int = {
        if (moneyRemaining == 0) 1
        else if (i >= coins.length) 0
        else if (coins(i) <= moneyRemaining) enumCoins(i, moneyRemaining - coins(i)) + enumCoins(i + 1, moneyRemaining)
        else enumCoins(i + 1, moneyRemaining)
      }
      enumCoins(0, money)
    }
  }
